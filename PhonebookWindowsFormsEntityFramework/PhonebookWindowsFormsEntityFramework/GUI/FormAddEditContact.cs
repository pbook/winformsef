﻿using PhonebookWindowsFormsEntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonebookWindowsFormsEntityFramework.GUI
{
    public partial class FormAddEditContact : Form
    {
        private Contact contact;
        public FormAddEditContact(Contact contact)
        {
            InitializeComponent();
            this.contact = contact;
        }

        private void FormAddEditContact_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("{0} Contact - Phonebook", contact.ID > 0 ? "Update" : "Add");

            textBoxName.Text = contact.Name;
            textBoxEmail.Text = contact.Email;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            contact.Name = textBoxName.Text;
            contact.Email = textBoxEmail.Text;

            this.DialogResult = DialogResult.OK;
        }
    }
}
