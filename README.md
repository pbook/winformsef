Phonebook is a project for educational purposes.
It is a desktop application implemented with Windows Forms and Entity Framework.

Prerequisites
Visual Studio 2013 or newer
Microsoft SQL Server 2012 or newer

Detailed description and instructions for the project are attached in the repo.